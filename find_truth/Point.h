#pragma once
#include <vector>
#include "CustomExceptions.h"
namespace Genetic_algorithm{
	typedef unsigned _int8 RowCount_TYPE;
	typedef unsigned _int8 ColumnCount_TYPE;
	
	//������� ����������� ���������� (�� ����� ������������ "-"). 
	class Point{
	private:
		RowCount_TYPE Row;
		ColumnCount_TYPE Column;
	public:
		Point(){ Row = 0; Column = 0; }
		Point(const Point & info_toCopy){
			Row = info_toCopy.GetRow();
			Column = info_toCopy.GetColumn();
		}
		Point(int row, int column){ Row = row; Column = column; }
		RowCount_TYPE GetRow() const{
			return Row;
		}
		ColumnCount_TYPE GetColumn() const{
			return Column;
		}
		//���������� �������, ����������� �� 1 ������� ���� ��������. ���������, ��� �������� �� ����� ���������� ����� ������� � ������ ����
		Point MoveDown() const { return Point(Row + 1, Column); }
		//���������� �������, ����������� �� 1 ������ ���� ��������. ���������, ��� �������� �� ����� ���������� ����� ������� � ������ ����
		Point MoveUp() const { 
			if (Row - 1 < 0)
				throw("Point::MoveUp : Can't move up");
			return Point(Row - 1, Column);
		}
		
		Point MoveRight() const { return Point(Row, Column + 1); }

		bool operator ==(const Point &CompareWith){
			if (Row == CompareWith.GetRow() && Column == CompareWith.GetColumn())
				return true;
			else return false;
		}
		bool operator >(const Point& CompareWith){
			if (Row == CompareWith.GetRow())
				return (Column > CompareWith.GetColumn());
			else return (Row > CompareWith.GetRow());
		}
		bool operator <(const Point& CompareWith){
			if (Row == CompareWith.GetRow())
				return (Column < CompareWith.GetColumn());
			else return (Row < CompareWith.GetRow());
		}
	};
	//���������� "�������" ������� ����������� ���������� -- ���, ��� ����� � ������� � ������� �������
	class ComparePoints_less{
	public:
		bool operator()(Point element1, Point element2){
			if (element1.GetRow() == element2.GetRow())
				return (element1.GetColumn() < element2.GetColumn());
			else return (element1.GetRow() < element2.GetRow());
		}
	};
	
	/* ------ �������� ��� �������� ����������� ���������� �� ���� ---------*/
	class Points_ImmutableContainer{
	private:		
		//���������, � ������� ���������� ��������������� �� ����������� ��������
		std::vector<Point> Container;
		RowCount_TYPE Rows_count; //���-�� ����� �����, � ������� ���������� ���� �� 1 ������� ����������		
	public:
		Points_ImmutableContainer(const std::vector <Point>& infos_vector);
		RowCount_TYPE GetRows_count(){ return Rows_count; }		
		//���������� ������, ��� ������� ������� ������������ � ����������, ��� ���������� �������� ����������� ���������� Does_not_contain_exception
		bool Contains(Point element); //true - ���� ������� ���������� � ����������
		std::vector<Point> GetAtRow(RowCount_TYPE row_index);
		Point GetValueAt(unsigned int index){
			if (index >= Container.size())
				throw ("Point_ImmutableContainer::GetValueAt : ����� �� ������� �������");
			return Container.at(index);
		}		
	};
}
