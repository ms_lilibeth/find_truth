#include"Chromosome.h"
#include "CustomExceptions.h"
#include<random>
#include "ml_digger.h"
namespace Genetic_algorithm{	
	// ����������� ����������� ���������� � ���������� �������
	Points_ImmutableContainer* Chromosome::MatchesTogether;
	Point Chromosome::U_location;
	Point Chromosome::T_location;
	bool Chromosome::StaticFieldsDefined;
	int Chromosome::Length;
	int Chromosome::MAX_FITNESS;	
	//-------------------------------------------------------------
		
	//������� ���������, �������� ������� ���������� � ���� ������
	Chromosome::Chromosome(){
		if (!StaticFieldsDefined)
			throw("Chromosome: static fields are not defined!");
		DigDowns_Chosen = std::vector <Point>();
		std::random_device rd;
		std::mt19937 generate(rd());
		std::uniform_int_distribution<> distributor;
		RowCount_TYPE curr_rownum = (MatchesTogether->GetValueAt(0)).GetRow();
		
		for (int i = 0; i < Length; i++){
			std::vector<Point> inCurrRow = MatchesTogether->GetAtRow(curr_rownum);
			distributor = std::uniform_int_distribution<>(0, inCurrRow.size() - 1);
			int index_chosen = distributor(generate);
			DigDowns_Chosen.push_back(inCurrRow.at(index_chosen));
			curr_rownum +=2;
		}		

		if (DigDowns_Chosen.size() != MatchesTogether->GetRows_count())
			throw ("���������� ��������� � DigDowns_Chosen �� ������������� ���������� �����");
		DigDowns_Chosen.shrink_to_fit();		
		CountFitness();
	}
	//������� ���������, �������� �� �� ���� ������ ���������: 0..delimiter -- �� �������, delimiter+1..MatchesTogether->GetQty() -- �� �������.
	Chromosome::Chromosome(const Chromosome& parent1, const Chromosome& parent2, RowCount_TYPE delimiter){
		if (!StaticFieldsDefined)
			throw("Chromosome: static fields are not defined!");
		DigDowns_Chosen = std::vector<Point>();
		if (delimiter >= Length || delimiter < 0)
			throw ("����� ����������� �� ������� ���������: Chromosome(const Chromosome& parent1, const Chromosome& parent2, RowCount_TYPE delimiter, Points_ImmutableContainer* matchsTogether)");
		try{
			for (int i = 0; i <= delimiter; i++){
				DigDowns_Chosen.push_back(parent1.GetElementAt(i));
			}
			for (int i = delimiter + 1; i < Length; i++){
				DigDowns_Chosen.push_back(parent2.GetElementAt(i));
			}
		}
		catch (std::out_of_range){
			throw("����� �� ������� ������������ ���������: Chromosome(const Chromosome& parent1, const Chromosome& parent2, RowCount_TYPE delimiter, Points_ImmutableContainer* matchsTogether)");
		}
		DigDowns_Chosen.shrink_to_fit();
		CountFitness();
	}
	void Chromosome::DefineStaticFields(Points_ImmutableContainer* matchesTogether, Point u_location, Point t_location){
		if (StaticFieldsDefined == true) 
			throw ("Chromosome::DefineStaticFields was already called");
		MatchesTogether = matchesTogether;
		Length = matchesTogether->GetRows_count();
		U_location = u_location;
		T_location = t_location;		
		
		/* +10 � fitness ����������� �� ������� �������� ����� junction ���������� �� ��� ������� ������ ���� � ��� �� 2 ��������: �� U � ������� ���� � �� ���������� ���� � T*/
		int connections_count = (MatchesTogether->GetRows_count() - 1) + 2;//(MatchesTogether->GetQty() - 1) ��������� ����� ����������, + 2 �������� (��.����);
		MAX_FITNESS = connections_count * 10 + 1; // + 1, ����� �����. �������������� �� �������� ����
		StaticFieldsDefined = true;
	}

	//������� ������������ ������������
	void Chromosome::CountFitness(){
		//������� ���������� ������
		int connections = 0;
		if (Digger::AreReachable(U_location, DigDowns_Chosen.at(0)))
			++connections;
		//�������� ������ DigDowns_Chosen � �������� �� ������������� �������, ��������� ����� ������� �������� � �����������
		for (unsigned int i = 0; i < DigDowns_Chosen.size() - 1; i++){
			if (Digger::AreReachable(DigDowns_Chosen.at(i), DigDowns_Chosen.at(i + 1)))
				++connections;
		}
		if (Digger::AreReachable(DigDowns_Chosen.back(), T_location))
			++connections; 
		Fitness = connections*10 + 1; //1 �����������, ����� �����. �������������� �� ��� ����� 0
		if (Fitness == 0)
			throw ("CountFitness: Fitness==0");
	}
	//�������: ��������� �������� ��������� ��������� ���������
	void Chromosome::Mutate()
	{
		std::random_device rd;
		std::mt19937 generate(rd());
		std::uniform_int_distribution<> dist;

		//�������� ���-�� ���������, ������ ���-�� ����������� ������ ��� ������������� �����.���.
		int mut_locuses_count = (MAX_FITNESS - Fitness)/10;
		//// �������� �������� ������ ����������� �������� (�� ������� ��������� ������ -- ��� ��������� ElementThatMustPresent)
		dist = std::uniform_int_distribution<>(0, Length - 2);
		for (int i = 0; i < mut_locuses_count; i++){
			int index = dist(generate);
			Point element = DigDowns_Chosen.at(index);
			RowCount_TYPE element_row = element.GetRow();
			//�� ���� �� ������ �������� ������ ��������� �������
			std::vector<Point> Row = MatchesTogether->GetAtRow(element_row);
			dist = std::uniform_int_distribution<>(0, Row.size() - 1);
			Point new_element = Row.at(dist(generate));
			DigDowns_Chosen.at(index) = new_element;
		}		
		//������ ������� ����������� ���������
		CountFitness();
	};	
}

