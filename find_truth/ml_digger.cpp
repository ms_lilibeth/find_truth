#include "ml_digger.h"
#include <set>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <random>

namespace Genetic_algorithm{	
	// ����������� ����������� ���������� � ���������� �������
	Points_ImmutableContainer* Digger::Layers;
	Points_ImmutableContainer* Digger::Dig_downs;
	//------------------------------------------------------
	Digger::Digger(str textfile_path){
		std::ifstream finput(textfile_path);
		if (!finput)
			throw("������ �������� �����");
		str str_tmp;
		char symbol_tmp;
		uchar row = 0;
		
		std::vector<Point> layers = std::vector<Point>();		
		std::vector<Point> dig_downs = std::vector<Point>();		

		while (!finput.eof()){
			finput >> str_tmp;			
			//� �������� strings_count -- ���� ����������, � ������ - ������������� ������� (dig_downs)
			for (uchar column = 0; column < (uchar)str_tmp.size(); column++){
				symbol_tmp = str_tmp.at(column);
				switch (symbol_tmp){
				case 'U':{
					U_location = new Point(row, column);
					break;
				}
				case 'T':{
					Truth_location = new Point(row, column);
					break;
				}
				case'-':{
					if (row % 2 == 0)
						dig_downs.push_back(Point(row, column));
					else layers.push_back(Point(row, column));
					break;
				}
				}
			}
			str_tmp.clear();
			++row;
		}
		sort(layers.begin(), layers.end(), ComparePoints_less());
		sort(dig_downs.begin(), dig_downs.end(), ComparePoints_less());
		Layers = new Points_ImmutableContainer(layers);
		Dig_downs = new Points_ImmutableContainer(dig_downs);		
		Chromosome::DefineStaticFields(Dig_downs, *U_location, *Truth_location);
	}
	
	//���������� ��������� ���� � ������ ��������� �������
	str Digger::FindPath(){
		return ConvertResult_toString(GeneticAlgorithm_perform());
	}
	//������� ��������� ���������
	std::vector<Chromosome> Digger::GetInitialPopulation(){	
		std::vector < Chromosome > result = std::vector < Chromosome >();
		for (int i = 0; i < POPULATION_SIZE; i++){
			Chromosome* chr_tmp = new Chromosome();
			result.push_back(*chr_tmp);
		}
		sort(result.begin(), result.end(), Chromosome::Fitness_greater);
		result.shrink_to_fit();
		return result;
	}
	//������������ ����� ��������� ����� ����������� �������������� ������ ���������
	std::vector<Chromosome> Digger::Reproduction(const std::vector<Chromosome>& parent_population){
		//����� ��������� ���� �� �������, ��� � ��������� ���������
		std::vector<Chromosome> new_population = std::vector<Chromosome>();
		Roulette_forReproduction roulette = Roulette_forReproduction(parent_population);
		
		//population.size() ��� �������� 2 ��������� ��������, �� ������� �������� ������ �������		
		for (unsigned int i = 0; i < parent_population.size(); i++){			
			int tmp_turnresult1 = roulette.TurnWheel();
			int tmp_turnresult2;
			do{
				 tmp_turnresult2 = roulette.TurnWheel();
			} while (tmp_turnresult2 == tmp_turnresult1);
			const Chromosome& mother = parent_population.at(tmp_turnresult1);
			const Chromosome& father = parent_population.at(tmp_turnresult2);	
			int delimiter = Chromosome::GetLength() / 2;
			for (unsigned int weakPoint = Chromosome::GetLength() - 2; weakPoint > 0; weakPoint--){
				if (!AreReachable(mother.GetElementAt(weakPoint), mother.GetElementAt(weakPoint + 1)) && !AreReachable(father.GetElementAt(weakPoint), father.GetElementAt(weakPoint + 1)))
				{
					delimiter = weakPoint;					
					break;
				}
			}			
			new_population.push_back(Chromosome(mother, father, delimiter));
		}
		sort(new_population.begin(), new_population.end(), Chromosome::Fitness_greater);
		new_population.shrink_to_fit();		
		return new_population;
	}
	//��������: ����� ������ �� ����� ���������
	std::vector<Chromosome> Digger::Breeding(const std::vector<Chromosome>& parent_population, const std::vector<Chromosome>& new_population){
		/* ����� ���������� ��� �������� ����� ���������, ����� ������� ��������, ���� ��.�����.�����. > ��������.
		�������� ������������� ���-�� ���������, ��� ���������� ��������� �������*/

		int tmp_mut_mustbe = (int)(MUTATIONS_PERCENT*new_population.size());
		if (tmp_mut_mustbe == 0)
			tmp_mut_mustbe = 1;
		
		std::vector <Chromosome> both_populations = std::vector<Chromosome>();
		//��������� ��� ��������� � ���� ������, ��������� �� ������������ ������������
		for (int i = 0; i < POPULATION_SIZE; i++){			
			both_populations.push_back(parent_population.at(i));
			both_populations.push_back(new_population.at(i));
		}
		std::sort(both_populations.begin(), both_populations.end(), Chromosome::Fitness_greater);
		//��������� ���������
		int index = 0;
		int index_is_duplicate;
	    
		while (index < both_populations.size() - 1){
			index_is_duplicate = index + 1;
			Chromosome* chr1 = &(both_populations.at(index));
			Chromosome* chr2 = &(both_populations.at(index_is_duplicate));
			while (index_is_duplicate < both_populations.size() && chr1->GetFitness() == chr2->GetFitness()){
				chr2 = &(both_populations.at(index_is_duplicate));
				if (chr2->IsEqual(*chr1)){
					both_populations.erase(both_populations.begin() + index_is_duplicate);
				}
				else {
					++index_is_duplicate;							
				}
			}
			++index;
		}
		//������ �������� �������� � ���������-���������
		std::vector<Chromosome> result = std::vector<Chromosome>();
		for (int i = 0; i < POPULATION_SIZE && i < both_populations.size(); i++){			
			result.push_back(both_populations.at(i));
		}
		if (result.size() < POPULATION_SIZE){
			int must_add = POPULATION_SIZE - result.size();
			for (int i = 0; i < must_add; i++){
				result.push_back(Chromosome());
			}
		}
		result.shrink_to_fit();
		std::random_device rd;
		std::mt19937 generate(rd());
		std::uniform_int_distribution<> dist(0, POPULATION_SIZE - 1);
		//��������� �������� (���-�� ������������ MUTATIONS_PERCENT) ��������
		for (int i = 0; i < tmp_mut_mustbe; i++){
			int index = dist(generate);
			result.at(index).Mutate();
		}
		sort(result.begin(), result.end(), Chromosome::Fitness_greater);
		return result;
	}
	
	//������� ������� ��� ������ �������� �� ��������� � �������������, ����� ����������������� Fitness
	Digger::Roulette_forReproduction::Roulette_forReproduction(const std::vector<Chromosome>& population){
		Points = std::vector<int>();
		int fitness_sum = 0; //����� ���������� "�������"
		for (unsigned int i = 0; i < population.size(); i++){
			fitness_sum += (population.at(i)).GetFitness();
			Points.push_back(fitness_sum); //�������� ����� �� "�������"
		}
		Points.shrink_to_fit();
		Length = fitness_sum; //�������� ��������� ����� � ���� ����� �������
	}
	//����� �� ������� �� ����� ������ � ������ �� ��������� ����������� ����������?
	//����������� ��� ���������, ����������� � ����� ��� �������� �������
	bool Digger::AreReachable(Point junctionElement1, Point junctionElement2){
		if (abs(junctionElement1.GetRow() - junctionElement2.GetRow()) != 2)
			throw("Chromosome::AreReachable: �������� ������ ���������� �� ��� ������� ������ ����");
		Point projection1, projection2;
		if (junctionElement1 < junctionElement2){
			projection1 = junctionElement1.MoveDown();
			projection2 = junctionElement2.MoveUp();
		}
		else {
			projection2 = junctionElement2.MoveDown();
			projection1 = junctionElement1.MoveUp();
		}
		if (projection1.GetRow() != projection2.GetRow())
			throw ("Chromosome::AreReachable : projection1.GetRow() != projection2.GetRow()");
		//���� �� �������� projection � ��������, ������� ���
		Point element_tmp, element_from, element_to;
		if (projection1 < projection2){
			element_from = projection1;
			element_to = projection2;
		}
		else {
			element_from = projection2;
			element_to = projection1;
		}
		element_tmp = element_from;
		do{
			if (!Digger::IsReliableInfo(element_tmp))
				return false;
			element_tmp = element_tmp.MoveRight();
		} while (element_tmp < element_to || element_tmp == element_to);

		return true;
	}
	//��������� ����� ���������
	int Digger::Roulette_forReproduction::TurnWheel(){
		std::random_device rd;
		std::mt19937 generate(rd());
		std::uniform_int_distribution<> distributor(1,Length);
		int rand_coordinate = distributor(generate); //��������� ����� � ��������� 1..Length
		for (unsigned int space_num = 0; space_num < Points.size(); space_num++){
			if (Points[space_num] >= rand_coordinate)
				return space_num;
		}
		throw("������������ ������ Digger::Roulette_forReproduction::TurnWheel()");
	}
	//���������� ������������� ���������
	Chromosome Digger::GeneticAlgorithm_perform(){
		std::vector<Chromosome> parent_population = GetInitialPopulation();		
		std::vector<Chromosome> new_population;
		//��������� ��������� ��������� �� ������� �������
		for (unsigned int index = 0; index < parent_population.size(); index++){
			if ((parent_population.at(index)).GetFitness() == Chromosome::GetMaxFitness())
				return parent_population.at(index);
		}
		Chromosome result;
		bool result_found = false;
		int iter_count = 1;
		int prev_fitness = 0, prev_fitness_count = 0;
		do{
			new_population = Reproduction(parent_population);			
			new_population = Breeding(parent_population,new_population); //����� �� �������� ����������� ��������
			int max_fitness = new_population.at(0).GetFitness();
			//��������� ����� ��������� �� ������� �������
			for (unsigned int index = 0; index < new_population.size(); index++){
				if ((new_population.at(index)).GetFitness() == Chromosome::GetMaxFitness()){
					result = new_population.at(index);
					result_found = true;
				}
			}
			//���������, ������� ��� �������� �������� ������������� �����.������������
			if (prev_fitness == max_fitness)
				++prev_fitness_count;
			else {
				prev_fitness_count = 0;
				prev_fitness = max_fitness;
			}
			//���� ����� �� ���������� � ������� 10 �������� (�������� ���������� ���������, ������ "������ �����"
			if (prev_fitness_count == 10){
				std::vector<Chromosome> new_blood = std::vector<Chromosome>();
				for (int i = 0; i < POPULATION_SIZE; i++){
					if (i < POPULATION_SIZE*0.05)
						new_blood.push_back(new_population.at(i)); //������ 5% ��������� ������� �������
					else new_blood.push_back(Chromosome()); //��������� -- �������� ����������������
				}
				parent_population = new_blood; //���������� ��
				prev_fitness_count = 0;
				prev_fitness = parent_population.at(0).GetFitness();				
			}
			else parent_population = new_population;
			++iter_count;
		} while (!result_found && iter_count <= MAX_ITERATIONS);
		
		if (!result_found)
			return GeneticAlgorithm_perform(); //�������� ��-�����
		else return result;
	}
	//�������������� ���������� � ������ ���������� �������
	str Digger::ConvertResult_toString(Chromosome Path){
		std::ostringstream result;
		Point projection1, projection2;
		
		for (int i = -1; i < Path.GetLength(); i++){
			int digs_count = 0;
			result << "dig down";
			if (i == -1){
				projection1 = U_location->MoveDown();
				projection2 = Path.GetElementAt(0).MoveUp();
			}				
			if (i == Path.GetLength() - 1){
				projection1 = Path.GetElementAt(i);
				projection2 = Truth_location->MoveUp();
			}
			if (i != -1 && i != Path.GetLength() - 1){
				projection1 = Path.GetElementAt(i).MoveDown();
				projection2 = Path.GetElementAt(i + 1).MoveUp();
			}		
			
			if (projection1 == projection2)
				result << std::endl;
			if (projection1 < projection2){
				digs_count = projection2.GetColumn() - projection1.GetColumn();
				result << ", dig right " << digs_count << std::endl;
			}
			if (projection1 > projection2){
				digs_count = projection1.GetColumn() - projection2.GetColumn();
				result << ", dig left " << digs_count << std::endl;
			}
		}
		return result.str();
	}		
}
