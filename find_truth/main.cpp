#include <iostream>
#include <string>
#include "ml_digger.h"
#include <clocale>
using namespace std;
int main( int argc, char *argv[] ) {
	setlocale(0, "rus");
        if( argc == 2 ) {     
                string userInput = argv[ 1 ];
				Genetic_algorithm::Digger *digger_instance;
                string answer;				
				try{
					digger_instance = new Genetic_algorithm::Digger(userInput);
					answer = digger_instance->FindPath();
				}
				catch (const char * p){
					cout << "������: " << p << endl;
					system("pause");
					return -1;
				}
				cout << answer << endl;
				//system("pause");
				delete digger_instance;
        }
        return 0;
}
