#pragma once
#include"Point.h"
#include <vector>
//#include <algorithm>

namespace Genetic_algorithm{
	/* ------- ��������� ---------- */
	class Chromosome{
	private:
		static Points_ImmutableContainer * MatchesTogether;
		std::vector <Point> DigDowns_Chosen; //������, �� ������� �������������� ������� �� ������ ����
		int Fitness; //����������� ������������		
		void CountFitness(); //������� ������������ ������������
		static Point U_location; 
		static Point T_location;		
		static bool StaticFieldsDefined;
		static int Length;		
		static int MAX_FITNESS;
	public:		
		//������� ���������, �������� ������� ���������� � ���� ������
		Chromosome();
		//������� ���������, �������� �� �� ���� ������ ���������: 0..delimiter -- �� �������, delimiter+1..matchsTogether->GetQty() -- �� �������.
		Chromosome(const Chromosome& parent1, const Chromosome& parent2, RowCount_TYPE delimiter);
		Point GetElementAt(unsigned int index) const { 
			if (index >= DigDowns_Chosen.size())
				throw ("Chromosome: ����� �� ������� �������");
			return DigDowns_Chosen.at(index); 
		};
		int GetFitness() const { return Fitness; }
		static int GetMaxFitness() { return MAX_FITNESS; }
		static int GetLength() { return Length; }
		void Mutate();
		static void DefineStaticFields(Points_ImmutableContainer* matchsTogether, Point u_location, Point t_location);
		static bool Fitness_greater(const Chromosome& chr1, const Chromosome& chr2){ return (chr1.Fitness > chr2.Fitness); }
		bool IsEqual(const Chromosome& comp_to){
			for (int i = 0; i < Length; i++){
				if (!(DigDowns_Chosen.at(i) == comp_to.GetElementAt(i)))
					return false;
			}
			return true;
		}		
	};
}
