#pragma once
#include <exception>
namespace Genetic_algorithm{
	class Does_not_contain_exception : std::exception{
	public:
		Does_not_contain_exception(const char* const& message) : exception(message){}
	};
}