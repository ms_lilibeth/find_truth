#include "Point.h"
#include <algorithm>
namespace Genetic_algorithm{

	Points_ImmutableContainer::Points_ImmutableContainer(const std::vector <Point>& infos_vector){
		Container = infos_vector;
		std::sort(Container.begin(), Container.end(), ComparePoints_less());
		Container.shrink_to_fit();
		//������� ���-�� ����� � ��������, � ������� ������� ���� �� 1 �������, ������������� ����������
		RowCount_TYPE prev_row = Container.at(0).GetRow();
		Rows_count = 1;
		for (int i = 0; i < Container.size(); i++){
			if (Container.at(i).GetRow() != prev_row){
				++Rows_count;
				prev_row = Container.at(i).GetRow();
			}				
		}		
	}

	bool Points_ImmutableContainer::Contains(Point element){
		bool result = std::binary_search(Container.begin(), Container.end(), element, ComparePoints_less());
		return result;
	}

	std::vector<Point> Points_ImmutableContainer::GetAtRow(RowCount_TYPE row_index){
		std::vector<Point> result = std::vector<Point>();
		for (unsigned int i = 0; i < Container.size(); i++){
			if (Container.at(i).GetRow() == row_index){
				result.push_back(Container.at(i));
			}
		}
		if (result.size() == 0)
			throw("Points_ImmutableContainer::GetAtRow : � ������ � ��������� ������� �������� �����������");
		result.shrink_to_fit();
		return result;
	}
}
