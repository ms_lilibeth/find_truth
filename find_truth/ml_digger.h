#pragma once
#include <string>
#include <vector>
#include "Point.h"
#include "Chromosome.h"

namespace Genetic_algorithm{
	
	typedef unsigned char uchar;
	typedef std::string str;
	
	/* ------- ���������� ������ ������� -------*/
	class Digger{
	public:
		Digger(str textfile_path);
		~Digger(){ delete U_location; delete Truth_location; delete Dig_downs; delete Layers; }
		str FindPath(); //���������� ��������� ���� � ������ ��������� �������		
		static bool IsReliableInfo(Point pnt){
			if (Layers->Contains(pnt) || Dig_downs->Contains(pnt)) 
				return true;
			else return false;
		}
		static bool AreReachable(Point element1, Point element2);
		
	private:
		const int MAX_ITERATIONS = 250;
		const int POPULATION_SIZE = 50;	
		const float MUTATIONS_PERCENT = 0.2;//������� ���������� ������ � ���������		
		
		Point* U_location;
		Point* Truth_location;
		static Points_ImmutableContainer* Layers; //��������, ������������ � �����
		static Points_ImmutableContainer* Dig_downs; //��������, ������������ ����� ������		
		Chromosome GeneticAlgorithm_perform();	//���������� ������������� ��������
		std::vector<Chromosome> GetInitialPopulation(); //������� ��������� ���������
		std::vector<Chromosome> Breeding(const std::vector<Chromosome>& parent_population, const std::vector<Chromosome>& new_population); //�������� (����� �� �������� ����������� ��������)
		std::vector <Chromosome> Reproduction(const std::vector<Chromosome>& parent_population); //������������ ����� ��������� ����� ����������� ������������� ������ ���������
		str ConvertResult_toString(Chromosome Path); //�������������� ���������� � ������ ���������� �������
				
		class Roulette_forReproduction{
		private:
			std::vector<int> Points;
			int Length; //����� �������: ����� fitness �� ���� ����������
		public:
			//������� ������� ��� ������ �������� �� ��������� � �������������, ����� ����������������� Fitness
			Roulette_forReproduction(const std::vector<Chromosome>& makeFor);
			//��������� ����� ���������. ���������� �� ������.
			int TurnWheel();			
		};
	};
}

